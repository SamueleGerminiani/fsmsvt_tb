interface fsm_intf(input clk, rst);
  
  //declaring the signals
  logic a;
  logic out1;
  logic out2;
  logic out3;
  
   modport dut (input clk, rst, a, output out1, out2, out3);
endinterface
