class transaction;
  //declaring the transaction items
  rand logic a;
  
  constraint a_c { a==0 || a==1; }; 
  
  //post-randomize function, displaying randomized values of items 
  function void post_randomize();
    $display("--------- [Trans] post_randomize ------");
    $display("\t a = %0h",a);
    $display("-----------------------------------------");
  endfunction
  
  //deep copy method
  function transaction do_copy();
    transaction trans;
    trans = new();
    trans.a  = this.a;
    return trans;
  endfunction
endclass
