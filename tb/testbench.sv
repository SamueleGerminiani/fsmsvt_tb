
//including interface and testcase files
`include "fsmInterface.sv"
`include "test.sv"

module tbench_top;
  
  logic clk,rst;

  
  //clock generation
  always #5 clk = ~clk;
  
  //reset Generation
  initial begin
    rst = 1;
    clk = 0;
    #5 rst =0;
  end
  
  
  //creatinng instance of interface, inorder to connect DUT and testcase
  fsm_intf intf(clk,rst);
  
  //Testcase instance, interface handle is passed to test as an argument
  test t1(intf);
  
  //DUT instance, interface signals are connected to the DUT ports
   fsm DUT (intf.dut);
  
  //enabling the wave dump
  initial begin 
    $dumpfile("fsm.vcd");
    $dumpvars(0, tbench_top, intf);
  end
endmodule
