#!/bin/sh
rm -rf work
vlib work
vlog -sv tb/testbench.sv rtl/fsm.sv
vsim -voptargs=+acc -c work.tbench_top -do "run -all; exit"
gtkwave fsm.vcd sfsm.gtkw 
