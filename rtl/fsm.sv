module fsm(fsm_intf intf);

// State encodings
typedef enum logic[1:0] {START=0,STATE_1,STATE_2,FINAL} State;

State state;

always_ff @(posedge intf.clk or intf.rst) begin
    if(intf.rst)begin
        state<=START;
    end else begin
        case (state)
            START:
                if (intf.a) begin
                    state<= STATE_1;
            end else begin
                state<= START;
            end
            STATE_1:
                if (intf.a) begin
                    state<= STATE_2;
            end else begin
                state<= START;
            end
            STATE_2:
                if (intf.a) begin
                    state<= FINAL;
            end else begin
                state<= START;
            end
            FINAL:
                if (intf.a) begin
                    state<= FINAL;
            end else begin
                state<= START;
            end
        endcase
    end
end

//output logic
assign intf.out1 = (state == STATE_1);
assign intf.out2 = (state == STATE_2);
assign intf.out3 = (state == FINAL);
endmodule
